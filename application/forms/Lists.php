<?php

class Application_Form_Lists extends Zend_Form
{

    public function init()
        {
            // Set the method for the display form to POST
            $this->setMethod('post');
     
            // Add an username element
            // $this->addElement('text', 'username', array(
            //     'label'      => 'Username:',
            //     'required'   => true,
            //     'filters'    => array('StringTrim')
            // ));
     
            // Add the content element
            $this->addElement('textarea', 'content', array(
                'label'      => 'Create your List (comma separated values):',
                'required'   => true,
            ));
     
            // Add the submit button
            $this->addElement('submit', 'submit', array(
                'ignore'   => true,
                'label'    => 'Create list!',
            ));
     
            // And finally add some CSRF protection
            $this->addElement('hash', 'csrf', array(
                'ignore' => true,
            ));
        }


}

