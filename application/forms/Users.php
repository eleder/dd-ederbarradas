<?php

class Application_Form_Users extends Zend_Form
{

    public function init()
        {
            // Set the method for the display form to POST
            $this->setMethod('post');
     
            // Add an username element
            $this->addElement('text', 'username', array(
                'label'      => 'Username:',
                'required'   => true,
                'filters'    => array('StringTrim')
            ));
     
            // Add the password element
            $this->addElement('password', 'password', array(
                'label'      => 'Password:',
                'required'   => true,
            ));
     
            // // Add a captcha
            // $this->addElement('captcha', 'captcha', array(
            //     'label'      => 'Please enter the 5 letters displayed below:',
            //     'required'   => true,
            //     'captcha'    => array(
            //         'captcha' => 'Figlet',
            //         'wordLen' => 5,
            //         'timeout' => 300
            //     )
            // ));
     
            // Add the submit button
            $this->addElement('submit', 'submit', array(
                'ignore'   => true,
                'label'    => 'Sign Up!',
            ));
     
            // And finally add some CSRF protection
            $this->addElement('hash', 'csrf', array(
                'ignore' => true,
            ));
        }


}

