<?php

class UsersController extends Zend_Controller_Action
{

    public function init()
    {
		/* Initialize action controller here */
		Zend_Session::start();

    }

    public function indexAction()
    {
		error_reporting(E_ALL);
		ini_set("display_errors", 1);
		$list_session = new Zend_Session_Namespace('list_session');
		if($list_session->is_logged==true){
			return $this->_helper->redirector('index','lists');
		}
	
		$users = new Application_Model_UsersMapper();
	
		$this->view->entries = $users->fetchAll();
    }

    public function signupAction()
    {
		$list_session = new Zend_Session_Namespace('list_session');
		if($list_session->is_logged==true){
			return $this->_helper->redirector('index','lists');
		}


		$request = $this->getRequest();
		$form    = new Application_Form_Users();
		
		if ($this->getRequest()->isPost()) {
			if ($form->isValid($request->getPost())) {
				$comment = new Application_Model_Users($form->getValues());
				$mapper  = new Application_Model_UsersMapper();
				$mapper->save($comment);
				return $this->_helper->redirector('index');
			}
		}
		
		$this->view->form = $form;
    }

    public function loginAction()
    {
    	$list_session = new Zend_Session_Namespace('list_session');
		if($list_session->is_logged==true){
			return $this->_helper->redirector('index','lists');
		}

        $request = $this->getRequest();
		$form    = new Application_Form_Users();
		
		if ($this->getRequest()->isPost()) {
			if ($form->isValid($request->getPost())) {
				$comment = new Application_Model_Users($form->getValues());
				$mapper  = new Application_Model_UsersMapper();
				$aux=$mapper->login($comment);

				if($aux==='true'){

					$list_session = new Zend_Session_Namespace('list_session');
					$list_session->is_logged=true;
					return $this->_helper->redirector('index','lists');
				}
				if($aux==='wrong_pass'){
					echo '<div class="error_message">'; 
					echo 'Wrong Password. Please try again';
					echo '</div class="error_message">';
				}
				if($aux==='wrong_user'){
					echo '<div class="error_message">'; 
					echo 'Username not found in the DB';
					echo '</div class="error_message">';
				}
				
				
			}
		}
		
		$this->view->form = $form;
    }

    public function logoutAction()
    {
    	Zend_Session::destroy();
    	return $this->_helper->redirector('index','index');
    }


}





