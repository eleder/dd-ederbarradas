<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */

    }

    public function indexAction()
    {
        // action body
        $list_session = new Zend_Session_Namespace('list_session');
		if($list_session->is_logged==true){
			return $this->_helper->redirector('index','lists');
		}
	
    }


}

