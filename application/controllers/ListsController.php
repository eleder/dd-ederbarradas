<?php

class ListsController extends Zend_Controller_Action
{

    public function init()
    {
		/* Initialize action controller here */
		Zend_Session::start();

    }

    public function indexAction()
    {
		error_reporting(E_ALL);
		ini_set("display_errors", 1);
		$list_session = new Zend_Session_Namespace('list_session');
	
		$lists = new Application_Model_ListsMapper();
	
		$this->view->entries = $lists->fetchAll();
    }

	public function addAction()
    {
		$list_session = new Zend_Session_Namespace('list_session');
	
		$request = $this->getRequest();
		$form    = new Application_Form_Lists();
			
		if ($this->getRequest()->isPost()) {
			if ($form->isValid($request->getPost())) {
				$comment = new Application_Model_Lists($form->getValues());
				$comment->userid=$list_session->userid;
				$mapper  = new Application_Model_ListsMapper();
				$mapper->save($comment);
				return $this->_helper->redirector('index');
			}
		}
		
		$this->view->form = $form;
    }

    public function updateAction()
    {
		$list_session = new Zend_Session_Namespace('list_session');
	
		$request = $this->getRequest();
		$form    = new Application_Form_Lists();
			
		if ($this->getRequest()->isPost()) {
			
				$comment = new Application_Model_Lists($form->getValues());

				$comment->content=$this->getRequest()->getPost('content', null);
				$comment->id=$this->getRequest()->getPost('table_id', null);
				$comment->userid=$list_session->userid;

				$mapper  = new Application_Model_ListsMapper();
				$mapper->save($comment);
		}
    }


    public function deleteAction()
    {
    	$request = $this->getRequest();
    		
    	if ($this->getRequest()->isPost()) {
    			$mapper  = new Application_Model_ListsMapper();
    			$mapper->delete($this->getRequest()->getPost('table_id', null));
    	}

    }

}





