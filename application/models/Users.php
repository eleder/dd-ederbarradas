<?php
	class Application_Model_Users
	{
		protected $_password;
		protected $_username;
		protected $_id;

		public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}
		}

		public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid users property');
			}
			$this->$method($value);
		}

		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid users property');
			}
			return $this->$method();
		}

		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}

		public function setPassword($text)
		{
			$this->_password = (string) $text;
			return $this;
		}

		public function getPassword()
		{
			return $this->_password;
		}

		public function setUsername($username)
		{
			$this->_username = (string) $username;
			return $this;
		}

		public function getUsername()
		{
			return $this->_username;
		}

		public function setId($id)
		{
			$this->_id = (int) $id;
			return $this;
		}

		public function getId()
		{
			return $this->_id;
		}
	}
?>