<?php
	class Application_Model_Lists
	{
		protected $_userid;
		protected $_content;
		protected $_id;

		public function __construct(array $options = null)
		{
			if (is_array($options)) {
				$this->setOptions($options);
			}
		}

		public function __set($name, $value)
		{
			$method = 'set' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid users property');
			}
			$this->$method($value);
		}

		public function __get($name)
		{
			$method = 'get' . $name;
			if (('mapper' == $name) || !method_exists($this, $method)) {
				throw new Exception('Invalid users property');
			}
			return $this->$method();
		}

		public function setOptions(array $options)
		{
			$methods = get_class_methods($this);
			foreach ($options as $key => $value) {
				$method = 'set' . ucfirst($key);
				if (in_array($method, $methods)) {
					$this->$method($value);
				}
			}
			return $this;
		}

		public function setUserid($text)
		{
			$this->_userid = (string) $text;
			return $this;
		}

		public function getUserid()
		{
			return $this->_userid;
		}

		public function setContent($content)
		{
			$this->_content = (string) $content;
			return $this;
		}

		public function getContent()
		{
			return $this->_content;
		}

		public function setId($id)
		{
			$this->_id = (int) $id;
			return $this;
		}

		public function getId()
		{
			return $this->_id;
		}
	}
?>