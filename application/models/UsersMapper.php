<?php

class Application_Model_UsersMapper
{
	protected $_dbTable;
	 
	    public function setDbTable($dbTable)
	    {
	        if (is_string($dbTable)) {
	            $dbTable = new $dbTable();
	        }
	        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
	            throw new Exception('Invalid table data gateway provided');
	        }
	        $this->_dbTable = $dbTable;
	        return $this;
	    }
	 
	    public function getDbTable()
	    {
	        if (null === $this->_dbTable) {
	            $this->setDbTable('Application_Model_DbTable_Users');
	        }
	        return $this->_dbTable;
	    }
	 
	    public function save(Application_Model_Users $users)
	    {
	        $data = array(
	            'username'   => $users->getUsername(),
	            'password' => $users->getPassword(),
	        );

	        $list_session = new Zend_Session_Namespace('list_session');
			$list_session->username=$data['username'];
			$list_session->password=$data['password'];
			$list_session->userid=$users->getId();
			$list_session->is_logged=true;
	 
	        if (null === ($id = $users->getId())) {
	            unset($data['id']);
	            $this->getDbTable()->insert($data);
	        } else {
	            $this->getDbTable()->update($data, array('id = ?' => $id));
	        }
	    }
	 
	    public function find($id, Application_Model_Users $users)
	    {
	        $result = $this->getDbTable()->find($id);
	        if (0 == count($result)) {
	            return;
	        }
	        $data[' $result->current()'];
	        $users->setId($row->id)	
	                  ->setUsername($row->username)
	                  ->setPassword($row->password);
	    }
	 
	    public function fetchAll()
	    {
	    	error_reporting(E_ALL);
		ini_set("display_errors", 1);
	        $resultSet = $this->getDbTable()->fetchAll();
	        $entries   = array();

	        foreach ($resultSet as $row) {

	            $entry = new Application_Model_Users();
	            $entry->setId($row->id)
	                  ->setUsername($row->username)
	                  ->setPassword($row->password);

	            $entries[]=$entry;

	        }
	        return $entries;
	    }

	    public function login(Application_Model_Users $users)
	    {

	        $resultSet = $this->getDbTable()->fetchAll();
	        $entries   = array();

	        $data = array(
	            'username'   => $users->getUsername(),
	            'password' => $users->getPassword(),
	        );

	        $is_user='wrong_user';

	        foreach ($resultSet as $row) {
	        	
	        	if($row->username===$data['username']){
	        		if($row->password===$data['password']){
	        			$is_user='true';
	        			$list_session = new Zend_Session_Namespace('list_session');
						$list_session->username=$row->username;
						$list_session->password=$row->password;
						$list_session->userid=$row->id;
						$list_session->is_logged=true;
						break;
	        		}
	        		else
	        		{
	        			$is_user='wrong_pass';
	        		}
	        	}
	        	else{
	        		$is_user='wrong_user';
	        	}
	        }
	        return $is_user;
	    }



}

