<?php

class Application_Model_ListsMapper
{
	protected $_dbTable;
	 
	    public function setDbTable($dbTable)
	    {
	        if (is_string($dbTable)) {
	            $dbTable = new $dbTable();
	        }
	        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
	            throw new Exception('Invalid table data gateway provided');
	        }
	        $this->_dbTable = $dbTable;
	        return $this;
	    }
	 
	    public function getDbTable()
	    {
	        if (null === $this->_dbTable) {
	            $this->setDbTable('Application_Model_DbTable_Lists');
	        }
	        return $this->_dbTable;
	    }
	 
	    public function save(Application_Model_Lists $lists)
	    {	
	
	        $data = array(
	            'user_id'   => $lists->getUserid(),
	            'content' => $lists->getContent(),
	        );
	 
	        if (null === ($id = $lists->getId())) {
	            unset($data['id']);
	            $this->getDbTable()->insert($data);
	        } else {
	            $this->getDbTable()->update($data, array('id = ?' => $id));
	        }
	    }
	 
	    public function find($id, Application_Model_Lists $lists)
	    {
	        $result = $this->getDbTable()->find($id);
	        if (0 == count($result)) {
	            return;
	        }
	        $data[' $result->current()'];
	        $lists->setId($row->id)	
	                  ->setUserid($row->user_id)
	                  ->setContent($row->content);
	    }
	 
	    public function fetchAll()
	    {
	    	error_reporting(E_ALL);
			ini_set("display_errors", 1);
	        $resultSet = $this->getDbTable()->fetchAll();
	        $entries   = array();
	        $list_session = new Zend_Session_Namespace('list_session');

	    	    foreach ($resultSet as $row) {
	      		  if(isset($list_session->userid)&&$list_session->userid==$row->user_id){
	        	
	        		$entry = new Application_Model_Lists();
	        		$entry->setId($row->id)
	        		      ->setUserid($row->user_id)
	        		      ->setContent($row->content);

	        		$entries[]=$entry;
	        	}
				

	        }
	        return $entries;
	    }

	    public function delete($id)
	    {	
	    	
	            $this->getDbTable()->delete($data, array('id = ?' => $id));
	    }

	   



}

